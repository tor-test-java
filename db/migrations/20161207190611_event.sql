DROP TABLE IF EXISTS events;
CREATE TABLE events (
	id INT(11) NOT NULL AUTO_INCREMENT,
	adapter TINYTEXT,
	remote_id TINYTEXT,
	event_date DATETIME,
	event TINYTEXT,
	description TEXT,
	impact TINYTEXT,
	unit TINYTEXT,
	country TINYTEXT,
	previous TINYTEXT,
	consensus TINYTEXT,
	actual TINYTEXT,
	releaser TEXT,
	url TEXT,
	created_at DATETIME,
	updated_at DATETIME,
    PRIMARY KEY (id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;
