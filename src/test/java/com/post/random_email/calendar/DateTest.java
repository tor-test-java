/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.calendar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import junit.framework.TestCase;

/**
 *
 * @author salvix
 */
public class DateTest extends TestCase {
	
	public void testConv1()
    {
		try{
//			String date = "25/04/2013";
			String date = "Dec 04, 21:00";
			
			Date currentDate = CalendarData.extractDate(date);
//			currentDate.g
			System.out.println("Date is ::"+currentDate);
        }
		catch(Exception e) {
            System.out.println("Error::"+e);
        }
    }
	
	public void testConv2() throws ParseException
	{
		String date = "2012-12-20";
		java.util.Calendar cal = CalendarData.parseDate(date);
		System.out.println(CalendarData.formatCalendar(cal));
		System.out.println(CalendarData.dateInXDays(cal, -1));
		System.out.println(CalendarData.dateInXDays(cal, 2));
	}
}
