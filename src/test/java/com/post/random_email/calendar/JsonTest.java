/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.calendar;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import org.codehaus.jackson.map.ObjectMapper;
/**
 *
 * @author salvix
 */
public class JsonTest extends TestCase{
	
	
	public static String userJson()
	{
		String s = "{\n" +
"  \"name\" : { \"first\" : \"Joe\", \"last\" : \"Sixpack\" },\n" +
"  \"gender\" : \"MALE\",\n" +
"  \"verified\" : false,\n" +
"  \"userImage\" : \"Rm9vYmFyIQ==\"\n" +
"}";
		return s;
	}
	
	public void testReadValue()
    {
		ObjectMapper o = new ObjectMapper();
		try {
			User u = o.readValue(userJson(), User.class);
			assertEquals(u.getName().getFirst(), "Joe");
			assertEquals(u.getName().getLast(), "Sixpack");
			assertEquals(u.getGender(), User.Gender.MALE);
		} catch (IOException ex) {
			Logger.getLogger(JsonTest.class.getName()).log(Level.SEVERE, null, ex);
		}
		
    }
}
