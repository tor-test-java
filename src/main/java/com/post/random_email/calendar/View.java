package com.post.random_email.calendar;

import org.javalite.activejdbc.LazyList;

public class View {

	public static String layout(String title, String content)
	{
		String head = "<!DOCTYPE html><html lang=\"en\"><head><meta charset=\"utf-8\">";
		String titleTag = "<title>"+title+"</title>";
		String style = "<link rel=\"stylesheet\" href=\"https://cdnjs.cloudflare.com/ajax/libs/milligram/1.2.0/milligram.min.css\">";
		String js = "<!--[if IE]><script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script><![endif]-->";
		String body = "</head><body id=\"home\">"+content+"</body></html>";
		return head+titleTag+style+js+body;
	}

	public static String layout(String content)
	{
		return layout("Calendar", content);
	}
	
	public static String calendarTable(LazyList<Event> events)
	{
		String v = "<table><thead><tr>";
		v += "<th>impact</th>";
		v += "<th>date</th>";
		v += "<th>country</th>";
		v += "<th>event</th>";
		v += "<th>actual</th>";
		v += "<th>forecast</th>";
		v += "<th>previous</th>";
		
		v += "</tr></thead><tbody>";
		for(Event e: events){
			v += "<tr>";
			v += "<td>"+e.get("impact")+"</td>";
			v += "<td>"+e.get("event_date")+"</td>";
			v += "<td>"+e.get("country")+"</td>";
			v += "<td>"+e.get("event")+"</td>";
			
			v += "<td>"+e.get("actual")+"</td>";
			v += "<td>"+e.get("consensus")+"</td>";
			v += "<td>"+e.get("previous")+"</td>";
			v += "</tr>";
		}
		v += "</tbody>";
		v += "</table>";
		return v;
	}
	

	public static String calendar(Page p){
		String v = "<h1>Calendar "+p.getId()+"</h1>";
		v += "<small>"+p.get("created_at");
		v += "<pre>"+p.get("calendar")+"</pre></small>";
		return layout(v);
	}

	public static String calendarSource(Page p){
		String v = "<h1>Calendar "+p.getId()+"</h1>";
		v += "<small>"+p.get("created_at");
		v += "<pre>"+p.get("source")+"</pre></small>";
		return layout(v);
	}
}