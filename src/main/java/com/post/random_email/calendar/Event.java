/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.calendar;

/**
 *
 * @author salvix
 */
import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;

public class Event extends Model {
	
	public static LazyList<Event> byDate(String date)
	{
		return byDateRange(date, date);
	}
	
	public static LazyList<Event> byDateRange(String fromDate, String toDate)
	{
		LazyList<Event> events = Event.where("event_date BETWEEN '"+fromDate+" 00:00:00' AND '"+toDate+" 23:59:59'").orderBy("event_date asc");;
		return events;
	}
}