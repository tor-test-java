package com.post.random_email.calendar;

import static com.post.random_email.calendar.View.calendarTable;
import static com.post.random_email.calendar.View.calendar;


/**
 * Hello world!
 *
 */
public class App 
{

    public static void main(String[] args)
    {
        // fetch("2016-11-29", "2016-11-30", "3");
        onStart();
        if(args.length >= 1){
            if(args[0].equals("fetch")){
                if(args[1].equals("today")){
                    CalendarData.fetchToday();
                }
                else if(args[1].equals("tomorrow")){
                    CalendarData.fetchTomorrow();
                }
                else if(args[1].equals("this-week")){
                    CalendarData.fetchThisWeek();
                }
                else if(args[1].equals("next-week")){
                    CalendarData.fetchNextWeek();
                }
            }
            else if(args[0].equals("convert")){
                CalendarData.convert();
            }
			else if(args[0].equals("ip")){
                System.out.println(Commands.whatIsMyIp());
            }
			else if(args[0].equals("tor")){
                System.out.println(Commands.torStatus());
            }
			System.exit(0);
        }
        else {
            launchWeb();
        }
    }

    public static void onStart()
    {
//		String date = "2016-11-10";
//		String content = calendar(CalendarData.findEvents(date));
//		System.out.println(content);
//		CalendarData.connect();
//        System.out.println(Event.byDateRange("2016-12-07", "2016-12-09"));
//		CalendarData.disconnect();
//		System.exit(0);
    }

    public static void launchWeb()
    {
        Web.init();
    }

}
