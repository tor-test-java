package com.post.random_email.calendar;

import biz.igg.oopdp.ff.tools.calendar.CalendarMap;
import biz.igg.oopdp.ff.tools.calendar.CalendarItem;
import org.javalite.activejdbc.Base;
import org.javalite.activejdbc.LazyList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CalendarData {


    public static void convert()
    {
        System.out.println("converting...");
        try {
            connect();
            LazyList<Page> pages = Page.where("calendar IS NULL");
            for(Page p: pages){
				System.out.println(p.getId());
				String source = p.get("source").toString();
				CalendarMap cm = Calendar.extractCalendar(source);
				saveCalendarMap(cm);
				String cal = cm.toString();
				p.set("calendar", cal);
				p.saveIt();
            }
            disconnect();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }
	
	public static void saveCalendarMap(CalendarMap cm)
	{
		for (CalendarItem ci : cm.values()) {
			Event e = build(ci);
			System.out.println(e);
			e.save();
		}
	}
	
	public static Date extractDate(String date) throws ParseException
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy");
        java.util.Calendar cal = java.util.Calendar.getInstance();
        String year = dateFormat.format(cal.getTime());
		date = date.replaceFirst(",", " "+year+", ");
		System.out.println(date);
		Date currentDate = new SimpleDateFormat("MMM d yyyy, HH:mm").parse(date);
		return currentDate;
	}
	
	public static Event build(CalendarItem ci)
	{
		Event e;
		Date date;
		e = Event.findFirst("remote_id = ?", ci.remoteId);
		if(e == null){
			e = new Event();
		}
		try {
			date = extractDate(ci.date);
		} 
		catch (ParseException ex) {
			return e;
		}
		String unit = ci.unit;
		String prev = ci.previous;
		String cons = ci.consensus;
		String act = ci.actual;
		if(unit == null) unit = "";
		if(prev == null) prev = "";
		if(cons == null) cons = "";
		if(act == null) act = "";
		e.set("remote_id", ci.remoteId);
		e.set("event_date", date);
		e.set("adapter", "MyFxBook");
		e.set("event", ci.event);
		e.set("unit", unit);
		e.set("impact", ci.impact);
		e.set("country", ci.country);
		e.set("previous", prev.replaceAll(unit, ""));
		e.set("consensus", cons.replaceAll(unit, ""));
		e.set("actual", act.replaceAll(unit, ""));
		return e;
	}
	
	

    public static String today()
    {
        return dateInXDays(0);
    }
	
	public static String tomorrow()
	{
		return dateInXDays(1);
	}
	
	public static String yesterday()
	{
		return dateInXDays(-1);
	}
	
	public static String dateInXDays(int nDays)
	{
        java.util.Calendar cal = java.util.Calendar.getInstance();
		return dateInXDays(cal, nDays);
	}
	
	public static String dateInXDays(java.util.Calendar cal, int nDays)
	{
		cal.add(java.util.Calendar.DATE, nDays); 
		return formatCalendar(cal);
	}
	
	
	public static String formatCalendar(java.util.Calendar cal)
	{
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(cal.getTime());
	}
	
	public static java.util.Calendar parseDate(String date) throws ParseException
	{
		Date parseDate = new SimpleDateFormat("yyyy-MM-dd").parse(date);
		java.util.Calendar cal = java.util.Calendar.getInstance();
		cal.setTime(parseDate);
		return cal;
	}
	
	
	
    public static void fetchToday()
    {
        fetch(today(), today(), "1");
    }

    public static void fetchTomorrow()
    {
    	fetch(today(), today(), "8");
    }

    public static void fetchThisWeek()
    {
    	fetch(today(), today(), "3");
    }

    public static void fetchNextWeek()
    {
    	fetch(today(), today(), "9");
    }

    public static void fetch(String start, String end, String period)
    {
        System.out.println("fetching...");
        try {
            String raw = Calendar.getCalendar(start, end, period);
            connect();
            saveContent(raw);
            disconnect();
        }
        catch(Exception e){
            System.out.println(e);
        }
    }

    public static void saveContent(String content)
    {
        Page p = new Page();
        p.set("source", content);
        p.saveIt();
    }

    public static void connect()
    {
        Base.open("com.mysql.jdbc.Driver", "jdbc:mysql://localhost/ff_cal", "ff_cal", "ff_cal");   
    }

    public static void disconnect()
    {
        Base.close();     
    }

    public static Page findPage(String id)
    {
        Page p = null;
        try {
            p = Page.findFirst("id = ?", id);
        }
        catch(Exception e){
            System.out.println(e);
        }
        return p;
    }
	
	public static LazyList<Event> findEvents(String date)
    {
        LazyList<Event> es = null;
        try {
            es = Event.byDate(date);
        }
        catch(Exception e){
            System.out.println(e);
        }
        return es;
    }
}

