package com.post.random_email.calendar;


import biz.igg.oopdp.ff.tools.curl.KeyValueList;
import biz.igg.oopdp.ff.tools.curl.KeyValue;
import java.io.IOException;
import biz.igg.oopdp.ff.tools.calendar.CalendarMap;
import biz.igg.oopdp.ff.tools.calendar.MyFxBook;
import biz.igg.oopdp.ff.tools.curl.CurlWrapper;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Random;

public class Calendar {


	public static String randIntSequence(int min, int max, int size) {

		String ret = "0.";
	    Random rand = new Random();
	    int randomNum;
	    for(int i = 0; i < size; i++)
		{
			randomNum = rand.nextInt((max - min) + 1) + min;
			ret += Integer.toString(randomNum);
		}
	    return ret;
	}

	public static KeyValueList calParams(String start, String end, String period)
	{
		start += "%2000:00";
		end += "%2000:00";
		KeyValueList params = new KeyValueList();
		params.add(new KeyValue("min", ""));
		params.add(new KeyValue("start", start));
		params.add(new KeyValue("end", end));
		params.add(new KeyValue("filter", "0-1-2-3_PEI-CNY-JPY-CZK-MXN-CAD-ZAR-AUD-NZD-CLP-GBP-NOK-ISK-CHF-RUB-ANG-ARS-INR-EEK-IDR-TRY-ROL-SGD-QAR-HKD-COP-DKK-SEK-BRL-EUR-HUF-PLN-USD-KRW-KPW"));
		params.add(new KeyValue("show", "show"));
		params.add(new KeyValue("type", "cal"));
		params.add(new KeyValue("bubble", "true"));
		params.add(new KeyValue("calPeriod", period));
		params.add(new KeyValue("rand", randIntSequence(0,9,17)));
		return params;
	}

	private static CalendarMap parseOutput(String content) throws IOException
	{
		MyFxBook fx = new MyFxBook(content);
		content = fx.cleanse(content);
		content = fx.extractCalendar(content);
		content = fx.fixSelfClosingTags(content);
		fx.parse(content);
		CalendarMap cm = fx.getCalendarMap();
		return cm;
	}


	public static String getQuery(KeyValueList kvl) throws UnsupportedEncodingException
	{
		StringBuilder result = new StringBuilder();
		boolean first = true;
		for (KeyValue pair : kvl)
		{
			if (first)
				first = false;
			else
				result.append("&");

			result.append(pair.key);
			result.append("=");
			result.append(pair.value);
		}
		return result.toString();
	}
	public static String getCalendar(String start, String end, String period)  throws Exception
	{
		KeyValueList kv = calParams(start, end, period);
		System.out.println(start+"|"+end+"|"+period);
		String params = getQuery(kv);
		String serviceUrl = "http://www.myfxbook.com/calendarEmailMinAlert.xml?";
		String output = CurlWrapper.get(
			serviceUrl+params, 
			"127.0.0.1", 
			9050
		);
		return output;
	}

	public static CalendarMap extractCalendar(String output) throws Exception
	{
		CalendarMap cm = parseOutput(output);
		return cm;
	}
}