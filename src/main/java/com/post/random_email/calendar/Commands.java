/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.post.random_email.calendar;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import biz.igg.oopdp.ff.tools.curl.CurlWrapper;

/**
 *
 * @author salvix
 */
public class Commands {
	
	
	public static String whatIsMyIp()
	{
		String ip = "";
		String output;
		try {
			output = CurlWrapper.get("http://whatismyip.org");
			Pattern pattern = Pattern.compile("<span style=\"color: blue; font-size: 36px; font-weight: 600;\">(.+?)</span>");
			Matcher matcher = pattern.matcher(output);
			matcher.find();
			int m = matcher.groupCount();
			if(m == 1){
				ip = matcher.group(1);
			}
			
		} catch (IOException ex) {
			
		}
		return ip;
	}
	
	
	public static String checkTorUsage(String output)
	{
		String ext = "";
		Pattern pattern = Pattern.compile("<title>\\s+(.+?)\\s+</title>");
		Matcher matcher = pattern.matcher(output);
		matcher.find();
		int m = matcher.groupCount();
		if(m == 1){
			ext = matcher.group(1).trim();
		}
		return ext;
	}
	
	public static String getTor()
	{
		String output = "";
		try {
			output = CurlWrapper.get("https://check.torproject.org", "127.0.0.1", 9050);			
		} 
		catch (IOException ex) {
			
		}
		return output;
	}
	
	public static String checkTorIp(String output)
	{
		String ext = "";
		Pattern pattern = Pattern.compile("<p>.*<strong>(.+?)</strong></p>");
		Matcher matcher = pattern.matcher(output);
		matcher.find();
		int m = matcher.groupCount();
		if(m == 1){
			ext = matcher.group(1).trim();
		}
		return ext;
	}
	
	public static String torStatus()
	{
		String ouput = getTor();
		return checkTorIp(ouput)+" - "+checkTorUsage(ouput);
	}
}
