package com.post.random_email.calendar;

import static spark.Spark.*;
import static com.post.random_email.calendar.View.*;

public class Web {
	
	public static void init(){
		
		before((request, response) -> {
			CalendarData.connect();
		});		
		
		after((request, response) -> {
			CalendarData.disconnect();
		});
		
		get("/", (request, response) -> {
    		response.redirect("/calendar/date/"+CalendarData.today());
			return "";
		});
		
				
		get("/hello", (req, res) -> "Hello World");
		

		get("/hello/:name", (request, response) -> {
    		String content = "Hello: " + request.params(":name");
    		return layout(content);
		});
		
		get("/calendar/date/:date", (request, response) -> {
			String date = request.params(":date");
			java.util.Calendar cal = CalendarData.parseDate(date);
    		String content = calendarTable(CalendarData.findEvents(date));
			String lControls = "<a href=\"/calendar/date/"+CalendarData.dateInXDays(cal, -1)+"\"><</a>";
			String rControls = "<a href=\"/calendar/date/"+CalendarData.dateInXDays(cal, +2)+"\">></a>";
    		return layout("<h1>"+lControls+"&nbsp;"+date+"&nbsp;"+rControls+"</h1>"+content);
		});

		get("/page/:id", (request, response) -> {
			String id = request.params(":id");
			Page p = CalendarData.findPage(id);
    		return calendar(p);
		});

		get("/page/:id/raw", (request, response) -> {
			String id = request.params(":id");
			Page p = CalendarData.findPage(id);
    		return calendarSource(p);
		});
	}
}